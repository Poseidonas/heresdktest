package com.chlaliotis.heresdkintro.di

import com.chlaliotis.heresdkintro.data.LoginDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object  DataSourceModule{
    @Provides
    @Singleton
    fun providesLoginDataSource(): LoginDataSource = LoginDataSource()
}
