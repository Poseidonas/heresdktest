package com.chlaliotis.heresdkintro.di

import com.chlaliotis.heresdkintro.data.LoginDataSource
import com.chlaliotis.heresdkintro.data.LoginRepository
import dagger.hilt.InstallIn
import dagger.Module
import dagger.Provides
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule{
    @Provides
    @Singleton
    fun provideesLoginRepository (loginDataSource: LoginDataSource) = LoginRepository(loginDataSource)


}

