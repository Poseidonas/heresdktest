package com.chlaliotis.heresdkintro.di

import com.chlaliotis.heresdkintro.di.preferences.PreferencesHelper
import com.chlaliotis.heresdkintro.prefernces.PreferencesHelperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
abstract class PersistenceModule{

    @Binds
    @Singleton
    abstract fun bindPreferencesHelper(preferencesHelperImpl: PreferencesHelperImpl): PreferencesHelper

}