package com.chlaliotis.heresdkintro.di.preferences
interface PreferencesHelper {

  fun saveToken(token: String)

  fun getToken(): String
}