package com.chlaliotis.heresdkintro

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.chlaliotis.heresdkintro.di.preferences.PreferencesHelper
import com.chlaliotis.heresdkintro.networking.NetworkStatusChecker
import com.chlaliotis.heresdkintro.networking.RemoteApi
import com.chlaliotis.heresdkintro.services.PingService
import com.chlaliotis.heresdkintro.utils.Actions
import com.chlaliotis.heresdkintro.utils.ServiceState
import com.chlaliotis.heresdkintro.utils.getServiceState
import com.google.android.gms.location.*
import com.here.android.mpa.common.*
import com.here.android.mpa.common.PositioningManager.*
import com.here.android.mpa.guidance.NavigationManager
import com.here.android.mpa.guidance.NavigationManager.MapUpdateMode
import com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.*
import com.here.android.mpa.search.ErrorCode
import com.here.android.mpa.search.GeocodeRequest
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.lang.ref.WeakReference
import javax.inject.Inject
import kotlin.properties.Delegates


@RequiresApi(Build.VERSION_CODES.M)
@AndroidEntryPoint
class MainActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {

    private val REQUEST_CODE_ASK_PERMISSIONS = 1

    private val LOCATION_PERM = 124
    private var speedUpStartTime = 0L
    private var speedUpEndTime = 0L

    private var speedDownStartTime = 0L
    private var speedDownEndTime = 0L
    private val speedSign = "m/s"
    private var distance = 0


    @Inject
    lateinit var remoteApi: RemoteApi

    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    private val networkStatusChecker by lazy {
        NetworkStatusChecker(getSystemService(ConnectivityManager::class.java))
    }


    private lateinit var fusedLocationProviderClient : FusedLocationProviderClient
    private lateinit var locationRequest :LocationRequest
    private lateinit var  locationCallback : LocationCallback

    private var isDone : Boolean by Delegates.observable(false){property, oldValue, newValue ->
        if(newValue == true){
            fusedLocationProviderClient.removeLocationUpdates(locationCallback)


        }else{


        }
    }

    private val RUNTIME_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.ACCESS_NETWORK_STATE
    )

    private var m_map: Map? = null
    private lateinit var editText : EditText
    private lateinit var marker : MapMarker
    private var mapFragment : MapFragmentView? = null
    private var m_mapRoute: MapRoute? = null
    private var m_navigationManager: NavigationManager? = null
    private var m_foregroundServiceStarted = false
    private var m_route: Route? = null



    private var paused = false
   // private lateinit var fusedLocationProviderClient : FusedLocationProviderClient





    override fun onPause() {
        super.onPause()

        paused = true;
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        super.onResume()
        paused = false;
        startLocationUpdates()

    }

    private fun startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper() )
    }

    override fun onDestroy() {

        super.onDestroy();
//        mapfragment.onDestroy()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        actionOnService(Actions.START)


        requestPermissions()
        askForLocalPermissions()
        createLocationRequest()
        locationCallback = object : LocationCallback(){



            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                if (!isDone) {
                    val speedToInt = locationResult?.lastLocation!!.speed.toInt()
                    Log.i("SPEED", speedToInt.toString())
                  //  calcSpeed(speedToInt)
                    distance = distance + speedToInt

                     speedText.text = "Velocity:" +speedToInt.toString() + speedSign + " Distance caputed:" +distance

                }

               // super.onLocationResult(locationResult)
            }

            override fun onLocationAvailability(p0: LocationAvailability?) {
                super.onLocationAvailability(p0)
            }
        }


    }

    private fun calcSpeed(speed: Int) {
        if(speed > 10){
            speedUpStartTime = System.currentTimeMillis()
            speedDownEndTime = System.currentTimeMillis()
            if(speedDownStartTime !=0L){
                val speedDownTime = speedDownEndTime - speedDownStartTime
                speedText.text = (speedDownTime/1000).toString() + speedSign
                speedDownStartTime = 0L

        } }else if(speed >=30){
            if(speedUpStartTime!= 0L){
             val speedUpTime = speedUpEndTime - speedUpStartTime
            speedText.text = ((speedUpTime/1000).toString()) + speedSign
            speedUpStartTime = 0L
            }
            speedDownStartTime = System.currentTimeMillis()

            }
        }




    private fun askForLocalPermissions() {

        if(hasLocationPermissions()){
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location :Location? ->

            }}else{
                EasyPermissions.requestPermissions(this,"need permissions to find the location and calculate the speed", LOCATION_PERM, Manifest.permission.ACCESS_FINE_LOCATION)
            }
        }

    private fun hasLocationPermissions(): Boolean {
        return  EasyPermissions.hasPermissions(this, android.Manifest.permission.ACCESS_FINE_LOCATION)

    }


    private fun createLocationRequest(){
        locationRequest = LocationRequest.create().apply {
            interval =1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

     }

    private fun requestPermissions() {
        if (!hasPermissions()) {
            ActivityCompat.requestPermissions(
                this,
                RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS
            )
        } else {
           initMapFragmentView()
        }
    }

    // Checks whether permission represented by this string is granted
    private fun String.permissionGranted(ctx: Context) =
        ContextCompat.checkSelfPermission(ctx, this) == PackageManager.PERMISSION_GRANTED

    private fun hasPermissions(): Boolean {
        /**
         * Only when the app's target SDK is 23 or higher, it requests each dangerous permissions it
         * needs when the app is running.
         */
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }

        return RUNTIME_PERMISSIONS.count { !it.permissionGranted(this) } == 0
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_ASK_PERMISSIONS -> {
                for (index in RUNTIME_PERMISSIONS.indices) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        var notGrantedMessage =
                            "Required permission ${permissions[index]} not granted."

                        /**
                         * If the user turned down the permission request in the past and chose the
                         * Don't ask again option in the permission request system dialog.
                         */
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                permissions[index]
                            )
                        ) {
                            notGrantedMessage += "Please go to settings and turn on for sample app."
                        }
                        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)

                        Toast.makeText(this, notGrantedMessage, Toast.LENGTH_LONG).show();
                    }
                }

                /**
                 * All permission requests are being handled. Create map fragment view. Please note
                 * the HERE Mobile SDK requires all permissions defined above to operate properly.
                 */

                initMapFragmentView()
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        TODO("Not yet implemented")
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

        if(EasyPermissions.somePermissionPermanentlyDenied(this, perms)){
            AppSettingsDialog.Builder(this).build().show()
        }
     }


    private fun initMapFragmentView() {



         mapFragment =  MapFragmentView(this)



    }


    override fun onRationaleAccepted(requestCode: Int) {
        TODO("Not yet implemented")
    }

    override fun onRationaleDenied(requestCode: Int) {
        TODO("Not yet implemented")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE){
            val yes = "Allow"
            val no = "Deny"
            Toast.makeText(this, "onActivityResult", Toast.LENGTH_LONG).show()
        }
    }
    private fun actionOnService(action: Actions) {
        if (getServiceState(this) == ServiceState.STOPPED && action == Actions.STOP) return
        Intent(this, PingService::class.java).also {
            it.action = action.name
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Log.i("PINGSERVICE","Starting the service in >=26 Mode")
                startForegroundService(it)
                return
            }
            Log.i("PINGSERVICE","Starting the service in < 26 Mode")
            startService(it)
        }
    }
}
