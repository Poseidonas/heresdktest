package com.chlaliotis.heresdkintro

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class App : Application(){

    companion object{
        private lateinit var instance : App

        val remoteApi by lazy{
           // buildRemoteApi()
        }

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}