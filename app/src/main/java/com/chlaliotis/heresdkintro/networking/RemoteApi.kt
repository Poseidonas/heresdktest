package com.chlaliotis.heresdkintro.networking

import com.chlaliotis.heresdkintro.model.UserProfile
import com.chlaliotis.heresdkintro.model.request.UserDataRequest
import com.chlaliotis.heresdkintro.model.response.Result
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

interface RemoteApi {

    fun loginUser(userDataRequest: UserDataRequest, onUserLoggedIn: (Result<String>) -> Unit)

    fun registerUser(userDataRequest: UserDataRequest, onUserCreated: (Result<String>) -> Unit)


   //  suspend fun getUserProfile(): Result<UserProfile>
}
