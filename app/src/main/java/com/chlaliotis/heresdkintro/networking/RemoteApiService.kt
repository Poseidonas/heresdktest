package com.chlaliotis.heresdkintro.networking


import com.chlaliotis.heresdkintro.model.request.UserDataRequest
import com.chlaliotis.heresdkintro.model.response.LoginResponse
import com.chlaliotis.heresdkintro.model.response.RegisterResponse
import com.chlaliotis.heresdkintro.model.response.UserProfileResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Holds the API calls for the the Alitis api  app.
 */
interface RemoteApiService {

    @POST("/api/register")
    fun registerUser(@Body request: UserDataRequest): Call<RegisterResponse>


    @POST("/api/login")
    fun loginUser(@Body request: UserDataRequest): Call<LoginResponse>

    @GET("/api/user/profile")
    suspend fun getMyProfile(): UserProfileResponse
}

