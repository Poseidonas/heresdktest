package com.chlaliotis.heresdkintro.model

import kotlinx.serialization.Serializable

class UserProfile(val email: String, val name: String, val numberOfNotes: Int)