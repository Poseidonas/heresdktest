package com.chlaliotis.heresdkintro.broadcastReceivers

import com.chlaliotis.heresdkintro.services.PingService
import com.chlaliotis.heresdkintro.utils.Actions
import com.chlaliotis.heresdkintro.utils.ServiceState
import com.chlaliotis.heresdkintro.utils.getServiceState

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import android.util.Log

class BootLoaderReceiver : BroadcastReceiver() {
    val TAG = "PINGSERVICE"

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED && getServiceState(context) == ServiceState.STARTED) {
            Intent(context, PingService::class.java).also {
                it.action = Actions.START.name
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Log.i("Ω","Starting the service in >=26 Mode from a BroadcastReceiver")
                    context.startForegroundService(it)
                    return
                }
                Log.i(TAG,"Starting the service in < 26 Mode from a BroadcastReceiver")
                context.startService(it)
            }
        }
    }
}


