package com.chlaliotis.heresdkintro

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.KeyEvent
import android.view.View;
import android.widget.Button;
import android.widget.EditText
import android.widget.Toast;
import androidx.annotation.RequiresApi
import com.here.android.mpa.common.*
import com.here.android.mpa.guidance.NavigationManager
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.*
import com.here.android.mpa.search.ErrorCode
import com.here.android.mpa.search.GeocodeRequest
import java.io.File
import java.lang.ref.WeakReference

const val MY_APP_ID = "o3PHMdjx8tNdxgUvEN0N"
const val MY_APP_CODE = "YiUYADOFL081Vb7jC8ZSmQ"
const val MY_LICENCE_KEY = "c7PQQ9BvIlzDREx9mjEHpj93DP5CsOvSz3z3kUnFPfwWCXj61Cdb+osHFWT4n9D5OZ76RQOHwZaAO8SVznHZ0h9zEZnAyDsuvZSq+NhHk7jxuE6O5pqomgfUiXFzWY4TU/R6eNmcp5OEu4L2lxjv6Rjzwch0U52DAkOS6HZlzvoWK3ew4GBMnGRqx7MG33vCZnOn6FJ9rEXjQo3rBCPtz/oKBCr1OZcDn8zOs6rAYgp94qvzECn+7OJe5BGMmj81fzYowdOHcZnX1L9ag3c8p1jQLm9g6WSKeTSZ1XkfzusaZGCLqU8BL2bB9tfCb7xnPpqt0Hfn9sLEMO2h9DEXsJjPSsbqjssq0Jtg+n2j0wEW7yRu5C4+PiR7ygPIWp5G+k56/SGJ+6a+cwDSYD5vwosGvohNS80Jcnth9O1eM5pudc7iVSV+Qc7tYTLmYhbFtjo9GN8ADkRp9kqd4wHuCSBVUmyGjD304drNim/dwQ5shg/EGBnfrbOGQgkYMplEelMGtMWnzHP9J3UP5JMCvOCCXOg/WzXwF4j1yvO4bEmFG1SNNxSDGaiNvHeM4p1eMg+pRtjDAINuR9u2PgByWsM/11aCJTfol+ViKd0QRIRlcksND7eVyZ6jShA28T07OABuZvPitUHbrKahSt5pZU8WxKn4Ow9r8/3HxjWfTE8="

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
class MapFragmentView(activity: MainActivity) {
    private val routingError = "Routing Error"
    private var m_mapFragment: AndroidXMapFragment? = null
        get() = m_activity.supportFragmentManager.findFragmentById(R.id.mapfragment) as AndroidXMapFragment?

   private var editText: EditText? = null
       get() = m_activity?.findViewById(R.id.query) as EditText


    private var navbutton: Button? = null
         get() = m_activity?.findViewById(R.id.navStartButton) as Button



    private var m_createRouteButton: Button? = null
    get()=  m_activity.findViewById<View>(R.id.naviCtrlButton) as Button

    private val m_activity: AppCompatActivity
    private var m_map: Map? = null
    private var m_mapRoute: MapRoute? = null


    private var destination: GeoCoordinate? =null

    private lateinit var marker : MapMarker
    private var mapFragment : MapFragmentView? = null
    private var m_navigationManager: NavigationManager? = null
    private var m_foregroundServiceStarted = false
    private var m_route: Route? = null

    private var paused = false

    private var positioningManager: PositioningManager? = null

    private val positionListener: PositioningManager.OnPositionChangedListener = object :
        PositioningManager.OnPositionChangedListener {
        override fun onPositionUpdated(
            method: PositioningManager.LocationMethod,
            position: GeoPosition?, isMapMatched: Boolean
        ) {
            // set the center only when the app is in the foreground
            // to reduce CPU consumption
            if (!paused && m_mapRoute == null) {
                m_map?.setCenter(
                    position!!.coordinate,
                    Map.Animation.NONE
                )
            }
        }

        override fun onPositionFixChanged(
            method: PositioningManager.LocationMethod,
            status: PositioningManager.LocationStatus
        ) {
        }

    }
    private val m_positionListener: NavigationManager.PositionListener =
        object : NavigationManager.PositionListener() {
            override fun onPositionUpdated(geoPosition: GeoPosition) {
                /* Current position information can be retrieved in this callback */
            }
        }
    private val m_navigationManagerEventListener: NavigationManager.NavigationManagerEventListener =
        object : NavigationManager.NavigationManagerEventListener() {
            override fun onRunningStateChanged() {
                Toast.makeText(activity, "Running state changed", Toast.LENGTH_SHORT).show()
            }

            override fun onNavigationModeChanged() {
                Toast.makeText(activity, "Navigation mode changed", Toast.LENGTH_SHORT).show()
            }

            override fun onEnded(navigationMode: NavigationManager.NavigationMode) {
                Toast.makeText(activity, "$navigationMode was ended", Toast.LENGTH_SHORT).show()
                stopForegroundService()
            }

            override fun onMapUpdateModeChanged(mapUpdateMode: NavigationManager.MapUpdateMode) {
                Toast.makeText(
                    activity, "Map update mode is changed to $mapUpdateMode",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onRouteUpdated(p0: Route) {
                Toast.makeText(activity, "Route updated", Toast.LENGTH_SHORT).show()
            }

            override fun onCountryInfo(s: String, s1: String) {
                Toast.makeText(
                     activity, "Country info updated from $s to $s1",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }




    private fun initMapFragment() {
        /* Locate the mapFragment UI element */




        if (m_mapFragment != null) {
            /* Initialize the AndroidXMapFragment, results will be given via the called back. */
            m_mapFragment!!.init { error ->
                if (error == OnEngineInitListener.Error.NONE) {
                    AlertDialog.Builder(m_activity).setMessage(
                       "Succesfully initialized"
                    )
                        .setTitle(R.string.engine_init_error)
                        .setNegativeButton(
                            R.string.cancel,
                            DialogInterface.OnClickListener { dialog, which -> m_activity.finish() })
                        .create().show()
                    m_map = m_mapFragment!!.map?.apply {
                        mapScheme = Map.Scheme.CARNAV_TRAFFIC_DAY
                        setZoomLevel(13.2)
                        setTilt(45F)
                        getMapTransitLayer()?.setMode(MapTransitLayer.Mode.EVERYTHING)
                        getPositionIndicator().setVisible(true)
                    }

                    /*
                                 * Set the map center to the 4350 Still Creek Dr Burnaby BC (no animation).
                                 */m_map!!.setCenter(
                        GeoCoordinate(49.259149, -123.008555, 0.0),
                        Map.Animation.NONE
                    )
                    positioningManager = PositioningManager.getInstance().apply {
                        addListener(
                            WeakReference<PositioningManager.OnPositionChangedListener>(positionListener))
                           start(PositioningManager.LocationMethod.GPS_NETWORK)
                    }

                    m_navigationManager = NavigationManager.getInstance().apply {
                        setMapUpdateMode(NavigationManager.MapUpdateMode.ROADVIEW)

                    }






                    /* Set the zoom level to the average between min and max zoom level. */



                } else {
                    AlertDialog.Builder(m_activity).setMessage(
                        """
                            Error : ${error.name}
                            
                            ${error.details}
                            """.trimIndent()
                    )
                        .setTitle(R.string.engine_init_error)
                        .setNegativeButton(
                            R.string.cancel,
                            DialogInterface.OnClickListener { dialog, which -> m_activity.finish() })
                        .create().show()
                }
            }
        }
    }

    private fun initCreateRouteButton() {
        m_createRouteButton = m_activity.findViewById<View>(R.id.naviCtrlButton) as Button
        m_createRouteButton!!.setOnClickListener(View.OnClickListener { /*
                     * Clear map if previous results are still on map,otherwise proceed to creating
                     * route
                     */
            if (m_map != null && m_mapRoute != null) {
                m_map!!.removeMapObject(m_mapRoute!!)
                m_mapRoute = null
            } else {
                /*
                         * The route calculation requires local map data.Unless there is pre-downloaded
                         * map data on device by utilizing MapLoader APIs, it's not recommended to
                         * trigger the route calculation immediately after the MapEngine is
                         * initialized.The INSUFFICIENT_MAP_DATA error code may be returned by
                         * CoreRouter in this case.
                         *
                         */
                createRoute()
            }
        })
       navbutton?.setOnClickListener(View.OnClickListener {
           startNavigation()
       })



    }

    /* Creates a route from 4350 Still Creek Dr to Langley BC with highways disallowed */
    private fun createRoute() {
        /* Initialize a CoreRouter */
        val coreRouter = CoreRouter()

        /* Initialize a RoutePlan */
        val routePlan = RoutePlan()

        /*
         * Initialize a RouteOption. HERE Mobile SDK allow users to define their own parameters for the
         * route calculation,including transport modes,route types and route restrictions etc.Please
         * refer to API doc for full list of APIs
         */
        val routeOptions = RouteOptions().apply {
                transportMode =
                RouteOptions.TransportMode.CAR
            setHighwaysAllowed(false)
            routeType =
                RouteOptions.Type.SHORTEST
            routeCount = 1
        }
        routePlan.routeOptions = routeOptions

        /* Define waypoints for the route */
        /* START: 4350 Still Creek Dr */
        val startPoint = RouteWaypoint(positioningManager?.position!!.coordinate)
        /* END: Langley BC */
        val rldestination = destination?.let { RouteWaypoint(it) }

        /* Add both waypoints to the route plan */routePlan.addWaypoint(startPoint)
        if (rldestination != null) {
            routePlan.addWaypoint(rldestination)
        }

         coreRouter.calculateRoute(
            routePlan,
            object : Router.Listener<List<RouteResult>, RoutingError> {
                override fun onProgress(i: Int) {
                    /* The calculation progress can be retrieved in this callback. */
                }

                override fun onCalculateRouteFinished(p0: List<RouteResult>?, p1: RoutingError) {
                    /* Calculation is done. Let's handle the result */
                    if (p1 == RoutingError.NONE) {
                        if (p0?.get(0)?.route != null) {
                            /* Create a MapRoute so that it can be placed on the map */
                            m_mapRoute = MapRoute(p0?.get(0)!!.route)
                            m_route = p0?.get(0)!!.route

                            /* Show the maneuver number on top of the route */m_mapRoute!!.isManeuverNumberVisible =
                                true

                            /* Add the MapRoute to the map */m_map!!.addMapObject(m_mapRoute!!)

                            /*
                                 * We may also want to make sure the map view is orientated properly
                                 * so the entire route can be easily seen.
                                 */
                            val gbb = p0[0].route
                                .boundingBox
                            m_map?.zoomTo(
                                gbb!!, Map.Animation.NONE,
                                Map.MOVE_PRESERVE_ORIENTATION
                            )
                        } else {
                            Toast.makeText(
                                m_activity,
                                "Error:route results returned is not valid",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            m_activity,
                            "Error:route calculation returned error code: $routingError",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }


            })
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun startForegroundService() {
        if (!m_foregroundServiceStarted) {
            m_foregroundServiceStarted = true
            val startIntent = Intent(m_activity, ForegroundService::class.java)
            startIntent.action = ForegroundService.START_ACTION
            m_activity.getApplicationContext().startForegroundService(startIntent)
        }
    }
    private fun stopForegroundService() {
        if (m_foregroundServiceStarted) {
            m_foregroundServiceStarted = false
            val stopIntent = Intent(m_activity, ForegroundService::class.java)
            stopIntent.action = ForegroundService.STOP_ACTION
            m_activity.getApplicationContext().startService(stopIntent)
        }
    }
    private fun startNavigation() {
        /* Configure Navigation manager to launch navigation on current map */
        m_navigationManager!!.setMap(
            m_map
        )

        /*
         * Start the turn-by-turn navigation.Please note if the transport mode of the passed-in
         * route is pedestrian, the NavigationManager automatically triggers the guidance which is
         * suitable for walking. Simulation and tracking modes can also be launched at this moment
         * by calling either simulate() or startTracking()
         */

        /* Choose navigation modes between real time navigation and simulation */
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(m_activity)
        alertDialogBuilder.setTitle("Navigation")
        alertDialogBuilder.setMessage("Choose Mode")
        alertDialogBuilder.setNegativeButton("Navigation", object :
            DialogInterface.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onClick(dialoginterface: DialogInterface?, i: Int) {
                m_navigationManager!!.startNavigation(m_route!!)
                m_map!!.tilt = 60f
                startForegroundService()
            }
        })
        alertDialogBuilder.setPositiveButton("Simulation", object :
            DialogInterface.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onClick(dialoginterface: DialogInterface?, i: Int) {
                m_navigationManager!!.simulate(m_route!!, 60) //Simualtion speed is set to 60 m/s
                m_map!!.tilt = 60f
                startForegroundService()
            }
        })
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        alertDialog.show()
        /*
         * Set the map update mode to ROADVIEW.This will enable the automatic map movement based on
         * the current location.If user gestures are expected during the navigation, it's
         * recommended to set the map update mode to NONE first. Other supported update mode can be
         * found in HERE Mobile SDK for Android (Premium) API doc
         */m_navigationManager!!.mapUpdateMode = NavigationManager.MapUpdateMode.ROADVIEW

        /*
         * NavigationManager contains a number of listeners which we can use to monitor the
         * navigation status and getting relevant instructions.In this example, we will add 2
         * listeners for demo purpose,please refer to HERE Android SDK API documentation for details
         */
        addNavigationListeners()
    }

    private fun addNavigationListeners() {

        /*
         * Register a NavigationManagerEventListener to monitor the status change on
         * NavigationManager
         */
        m_navigationManager!!.addNavigationManagerEventListener(
            WeakReference(
                m_navigationManagerEventListener
            )
        )

        /* Register a PositionListener to monitor the position updates */m_navigationManager!!.addPositionListener(
            WeakReference(m_positionListener)
        )

}
    private fun initTextquery(){
        editText = m_activity?.findViewById(R.id.query) as EditText

        editText?.setOnKeyListener(View.OnKeyListener { _, keyCode, keyevent ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && keyevent.action == KeyEvent.ACTION_UP) {
                dropMarker(editText?.text.toString())
                editText?.setText("")
                return@OnKeyListener true
            }
            false
        })
    }


    private fun dropMarker(query: String) {
        if (::marker.isInitialized) {
            m_map?.removeMapObject(marker)
        }
        val tracy = GeoCoordinate(37.7397, -121.4252)
        val request = GeocodeRequest(query).setSearchArea(tracy, 5000)
        request.execute { results, error ->
            if (error != ErrorCode.NONE) {
                Log.e("HERE", error.toString())
            } else {
                if (!results.isNullOrEmpty()) {
                        marker = MapMarker()
                        marker.coordinate = GeoCoordinate(
                            results.get(0).location?.coordinate!!.latitude,
                            results.get(0).location?.coordinate!!.longitude,
                            0.0
                        )


                        m_map?.addMapObject(marker)
                        destination = results.get(0).location!!.coordinate


                    if (m_map != null && m_mapRoute != null) {
                        m_map?.removeMapObject(m_mapRoute!!);
                        m_mapRoute = null;
                    }


                  //  results.get(0).location?.coordinate?.let { createRoute(it) };

                }
            }
        }
    }



    init {
        m_activity = activity
        initMapFragment()

        initCreateRouteButton()
        initTextquery()
    }






}